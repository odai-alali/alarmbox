import os
from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit

BACKEND_PORT = os.environ.get('PORT', 11711)

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret_key'
app.config['TEMPLATES_AUTO_RELOAD'] = True
socketio = SocketIO(app)

@app.route('/')
def index():
    return render_template('index.html')

@socketio.on('connect')
def handle_connect():
    print('Client connected')

@socketio.on('disconnect')
def handle_disconnect():
    print('Client disconnected')

@app.route('/tweet', methods=['POST'])
def handle_post():
    text = request.form.get('text')
    socketio.emit('new-tweet', text)
    return {
        'message': 'Tweet sumbitted successfully'
    }

if __name__ == '__main__':
    socketio.run(app, port=BACKEND_PORT, host='0.0.0.0')
