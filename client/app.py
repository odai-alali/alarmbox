import socketio

sio = socketio.Client()
sio.connect('http://localhost:11711')

@sio.on('connect')
def on_connect():
  print('Connected to server')

@sio.on('new-tweet')
def on_message(data):
  # ToDo: Add your code here to handle new tweets
  print('Received message:', data)

@sio.on('disconnect')
def on_disconnect():
  print('Disconnected from server')

sio.wait()
