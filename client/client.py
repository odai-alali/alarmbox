import socket
from google.cloud import texttospeech
import config
import os
import time
from pydub import AudioSegment
from pydub.playback import play
#import RPi.GPIO as GPIO

#GPIO.setwarnings(False)
# Set up GPIO mode
#GPIO.setmode(GPIO.BCM)

# Set up GPIO pins
#GPIO.setup(18, GPIO.OUT)

def play_wav_file(file_path):
    sound = AudioSegment.from_file(file_path, format="wav")
    play(sound)


# Set environment variable to path of service account key file
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = config.GOOGLE_APPLICATION_CREDENTIALS

host = "127.0.0.1"
port = 5050
timeout_seconds = 5
max_failures = 10
# Initialize a Text-to-Speech client with the project ID
textToSpeachClient = texttospeech.TextToSpeechClient()

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as clientSocket:
    failCounter = 0
    while failCounter < max_failures:
        try:
            clientSocket.connect((host, port))
            # socket.create_connection(host, timeout=GLOBAL_DEFAULT, source_address=None, *, all_errors=False) # TODO refactor to use higher level socket api.
            print("Connected to the server!")
            connected = True
        except Exception as e:
            print(f"Exception: {e}")
            if e.errno == 106:
                # print("Client already c")
                clientSocket.close()
                clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                # connected = True
            else:
                time.sleep(timeout_seconds)
                failCounter += 1
                print(f"Failed to connect to the server. Retrying... ({failCounter}/10)")
                continue
        failCounter = 0
        while connected:
            data = clientSocket.recv(1024)
            if not data:
                print("Connection closed by the server")
                connected = False
                break
            print(f"Received data from the server: {data.decode()}")

            # Set the text input to be synthesized
            synthesis_input = texttospeech.SynthesisInput(text=data.decode())

            # Set the voice parameters
            voice = texttospeech.VoiceSelectionParams(
                language_code="en-US",
                name="en-US-Wavenet-D"
            )

            # Set the audio parameters
            audio_config = texttospeech.AudioConfig(
                audio_encoding=texttospeech.AudioEncoding.LINEAR16
            )

            # Perform the text-to-speech request
            response = textToSpeachClient.synthesize_speech(
                input=synthesis_input,
                voice=voice,
                audio_config=audio_config
            )

            # Save the audio to a WAV file      
            filename = "output.wav"
            with open(filename, "wb") as out:
                # Write the response to the output file.
                out.write(response.audio_content)
                print('Audio content written to file "output.wav"')

            print("Saved output to output.wav")
          #  GPIO.output(18,1)
            time.sleep(5)

            #play wav file
            output_file_path = "output.wav"
            noise_file_path = "Shiphorn.wav"
            play_wav_file(noise_file_path)
            play_wav_file(output_file_path)
            time.sleep(5)
          #  GPIO.output(18,0)


            



print("Disconnected from the server.")
