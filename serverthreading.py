import os
import json
import config
import socket
import queue
import threading
import requests
import urllib
import time
from langdetect import detect
import re
import base64


host = "0.0.0.0"
port = 5050

key = "ALARM_PHONE_KEY"

# Shared queue between the threads to store the tweets
data_queue = queue.Queue()
clients = []


class TwitterThread(threading.Thread):
    def __init__(self):
        super().__init__()
        self.shouldPoll = True

    def get_stream(self):
        # TextOfTweet = "" # initialize variable to store the tweet text
        TextOfTweet = " Fuck you #Mask"
        # delete hashtags from text of tweet
        hashtags = re.findall(r'#(\w+)', TextOfTweet)
        if hashtags:
            for hashtag in hashtags:
                TextOfTweet = TextOfTweet.replace('#' + hashtag, hashtag)

        data_queue.put(TextOfTweet.encode())
        print(TextOfTweet)
        # self.handelClients()
        self.broadcastToClients()

        print('should call broadcasting')

    def run(self):
        self.get_stream()

    def broadcastToClients(self):
        while not data_queue.empty():
            data = data_queue.get()
            for client in clients[:]:
                try:
                    print("broadcasting to", client, '\n', data)
                    client.sendall(data)
                except Exception as e:
                    print("Error broadcasting to client", client, e)
                    clients.remove(client)


if __name__ == "__main__":
    # Create a socket object
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Bind to the port
    s.bind((host, port))

    # Queue up to 5 requests
    s.listen(5)
    print("Server started on port", port)
    twitter_thread = TwitterThread()
    twitter_thread.start()
    try:
        while True:
            # Establish a connection
            conn, addr = s.accept()
            #
            data = conn.recv(1024).decode()  # Receive data from client
            if data.strip() == key:
                clients.append(conn)  # Add client to the list
                conn.sendall(b"OK")
                print(f"Accepted client: {conn.getpeername()}")
            else:
                conn.sendall(b"Invalid key. Disconnecting...")  # Send error message
                conn.close()  # Disconnect client
                print(f"Rejected client")

            # Read data from command line and send to the connected client
            command = input("Enter data to send (or 'q' to quit): ")
            if command == 'q':
                break
            command_data = command.encode()
            for client in clients:
                try:
                    client.sendall(command_data)
                except Exception as e:
                    print("Error sending data to client", client, e)
                    clients.remove(client)
                    client.close()
    except KeyboardInterrupt:
        twitter_thread.shouldPoll = False

        s.close()