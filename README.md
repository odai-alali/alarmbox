# Alarm-Box


This is a project to build a box that will play a sound when a button is pressed. The box will be powered by a Raspberry Pi and will be controlled by a web interface.


## server
### docker
* `docker build -t ab-server .`
* `docker run --rm -p 5050:5050 ab-server`

## client
pip install pydub
pip install google-cloud-texttospeech
docker compose up --build

#host = "192.168.43.203"c

